# SAE Système
Rédigé par BOURDERE ANDREOU Nathan, Etudiant INFO 11A.
Guide d'installation d'une WSL2(Windows Subsystem for Linux version 2) sous windows 10:
Tout d'abord,
-Assurez vous d'avoir un Windows 10 famille avec la dernière version possible (ou supportable) pour votre PC.
-Cliquez sur "taper ici pour rechercher" qui se trouve juste à côté de l'icone windows en bas à gauche de l'écran de votre PC.
-Ecrivez "Powershell" et éxecutez le en mode administrateur.
-Dans le Powershell écrivez (ou copiez) "dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all"
Cela permets d'activer la feature donc pour traduire, la caractéristique WSL de Microsoft.
-Ecoutez ce que dit Windows et redémarrez votre Ordinateur.
-Bravo vous avez WSL! mais seulement la version 1.0 et elle dorénavent pratiquement inutile.
-Donc tout d'abord, suivez ces différentes consignes:
-Téléchargez et Installez https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi pour posséder un noyau Linux pour WSL2.
-Ensuite éteignez votre ordinateur et appuyez longuement sur F2 au **DEMARRAGE** de celui-ci.
Cliquez sur Résolution des problèmes.
Cliquez sur Options avancées.
Cliquez sur Paramètres du microprogramme UEFI.
Cliquez sur Redémarrer.
Vous êtes dans le BIOS maintenant.
-Déplacez vous dans "Advanced" donc Avancé pour les paramètres avancés.
-"Scrollez" jusqu'à trouver un paramètre de virtualisation, Mettez le en Enabled si il est Disabled, si il l'est dèjà, remettez le en Disabled et tout de suite en Enabled pour s'assurer que Windows est bien la virtualisation d'activé.
-Maintenant quittez le BIOS( par ailleurs BIOS veut dire Basic Input Output System)
-Demarrez normalement Windows et installez un Ubuntu ou Debian ou Kali(suivant ce que vous voulez) dans le Windows Store.
- Re-ouvrez Powershell en Administrateur et écrivez "dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all"
Maintenant vous avez activé la plateforme Virtuelle pour support de votre WSL2!
-Si Windows vous dit de redemarrer, faites-le encore.
-Ecrivez "wsl --set-default-version 2" pour avoir WSL2, vérifiez avec "wsl --list --verbose" pour s'assurer qu'elle soit en WSL2 (votre distribution)
si il ne veut pas, c'est peut être qu'il faut lancer votre distribution avant! faites-donc l'instruction suivante avant celle-ci.
-Lancez la distribution Ubuntu/Debian/Kali que vous avez choisis et finie d'installer.
-Normalement Il vous demande de créer un nom d'utilisateur, sans majuscules, que des lettres alphabétiques et sans espaces.
-Ecrivez votre mot de passe, rappelez vous-en!
-Normalement vous avez WSL2 d'installé sur votre PC! Bravo et maintenant vous pouvez en profiter pour utiliser Linux sous Windows!
(ps: N'oubliez pas de regarder les fichiers de configuration de ce dépôt git pour savoir comment installer et configurer quelques essentiels pour 
un informaticien).
